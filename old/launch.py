import PySimpleGUI as gui

gui.theme('DarkAmber')   # change the theme to dark

mylist = ["apple", "banana"]
mylist.append("kiwi")

dictionary = {
    "car" : ["doors", "motors", "steeringwheel"]
}

chapter = ["once", "upon", "a", "time"]
counter = 0
while chapter[counter] != "a":
    counter += 1
if counter == chapter.__len__():
    print("we didn't find the word!")
else:
    print(chapter[counter-1])
    print(chapter[counter+1])

chapter_dictionary = {
    "once": [0],
    "upon": [1],
    "a":    [2, 12, 44],
    "time": [3]
}

counter = chapter_dictionary["a"][0]
while counter < chapter.__len__():
    print(chapter[counter])
    counter += 1

chapter[chapter_dictionary["a"][0]+1]

print(chapter[chapter_dictionary["a"][0] + 1])
print(chapter[counter + 1])

# myimage = [[0,0,0,0],
#            [0,1,1,0],
#            [0,1,0,0],
#            [0,0,0,0],
#            ]
# pixel = myimage[0][2]
# pixel = myimage[2][2]
# myimage = [0, 0, 0, 0,
#            0, 1, 1, 0,
#            0, 1, 0, 0,
#            0, 0, 0, 0]
# myimage[5]
# myimage[5+width+1]
# find_me[4+width+width+2]
# position + moveright() + movedown() + movedown()

#larsvariable=mylist.pop()
#print(mylist.pop())
#print(mylist)
#print(larsvariable)

# Variable "Layout" stores all the stuff inside your window in a list
layout = [[gui.Text('Some text on Row 1')],
          [gui.Text('Enter something on Row 2'), gui.InputText()],
          [gui.Button('Ok', key="OK_BUTTON"), gui.Button('Cancel')]]

# Create the Window
window = gui.Window('Window Title', layout)

test = (42, 5)

while True:
    print(window.read(timeout=0))

# Event Loop to process "events" and get the "values" of the inputs
while "banana" in mylist:
    event, values = window.read()
    if event in (None, 'Cancel'):   # if user closes window or clicks cancel
        del mylist[1]
    print('You entered ', values[0])

window.close()