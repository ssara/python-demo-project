You are given a DNA sequence: 
 
```python 
seq =  
"GATGTCGGATTTGAGTTCTTAAGTGAGAAGTACATGCGTACACGTCTTTTCAAGCCCCATACTCTGTCTAGCAATGTACTCTTCCAGGCTCGTGTGATCCTGAAGCTCTATTATATAACAGGGTGTTAGCCCCCGATGACTCGATTCA" 
 
### 
# Side note: 
# You can generate a random DNA sequence like this: 
import numpy as np 
seq = ''.join(np.random.choice(['A', 'T', 'C', 'G'], 1000))   # 1000 is  
the number of nucleotides 
### 
 
``` 
 
your job is to write a function named `kmer` with two parameters (`seq,  
k`): `seq` is the DNA sequence and `k` is an integer. The function  
should return a dictionary with the keys being all the subsequences of  
size `k` and the corresponding value being the number of times it occurs  
in seq. 
 
 
--------------------------------- 
 
   You are given a data file (data.txt) with the following format (see  
example below): 
 
   - First line contains the columns names. We will not use them 
   - If a line starts with a #, it is a comment and should be ignored 
   - Other lines contain data in this format: mouse_name (a string), day  
(a number ; we will not use it here), weight (a number) 
 
   Your task: 
 
   1. Read the file content so that you can get per mouse, all their  
weights. 
   2. From the extracted data, compute the average weight per animal 
   3. Compute the average weight of all animals 
 
Do this first without any additional package, then you may use extra  
packages, like numpy for example. 
 
   ``` 
   mouse, day, weight 
   m1, 1, 31 
   m2, 1, 30 
   m1, 2, 30 
   m2, 2, 31 
   # forgot mouse 3 
   m3, 1, 29 
   m3, 2, 30 
   m1, 3, 28 
   m2, 3, 31 
   m3, 3, 29 
   m1, 4, 31 
   m2, 4, 30 
   m3, 4, 28 
   ``` 