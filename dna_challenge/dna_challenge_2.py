# BAE HINT: THIS IS HOW TO READ A FILE: https://stackoverflow.com/questions/3277503/how-to-read-a-file-line-by-line-into-a-list
with open("filename.txt") as file:
    content = file.readlines()

# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]
