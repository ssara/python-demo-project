# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_core/py_basic_ops/py_basic_ops.html#basic-ops
from os import listdir
from os.path import isfile, join
import sys
import cv2

print("path: \"" + sys.argv[1] + "\"")
all_content = listdir(sys.argv[1])
onlyfiles = [f for f in all_content if isfile(join(sys.argv[1], f))]

img = cv2.imread(join(sys.argv[1], onlyfiles[1234]))
cv2.imshow(onlyfiles[1234], img)
cv2.waitKey(0)
cv2.destroyAllWindows()